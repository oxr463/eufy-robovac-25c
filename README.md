# eufy - RoboVac 25C

_Wi-Fi Connected Robot Vacuum_

```yml
model: T2123
```

## License

SPDX-License-Identifier: [GPL-2.0-or-later](https://spdx.org/licenses/GPL-2.0-or-later.html)

## Reference

- [eufy | RoboVac 25C](https://www.eufylife.com/products/variant/robovac-25c/T2123111)
